//import strings.StringNewMethods;

import private_method_interface.Impresora;
import private_method_interface.Imprimible;

public class Main {

    public static void main(String[] args) {
        System.out.println("hello world");
        Imprimible i= new Impresora();
        i.encender();
        i.imprimir();
        i.apagar();

    }

    /*private static void Strings() {
        StringNewMethods stringNewMethods = new StringNewMethods();
        stringNewMethods.repeat();
        stringNewMethods.stripVsTrim();
        stringNewMethods.blank();
        stringNewMethods.multilines();
        stringNewMethods.compareequals();
    }*/
}
