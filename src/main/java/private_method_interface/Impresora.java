package private_method_interface;

public class Impresora implements Imprimible {

    @Override
    public void encender() {
        System.out.println("encendiendo el dispositivo");
    }

    @Override
    public void imprimir() {
        System.out.println("imprimiendo con la impresora");
    }
}
