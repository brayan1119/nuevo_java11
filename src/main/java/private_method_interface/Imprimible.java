package private_method_interface;

public interface Imprimible {

    public default void encender() {
        formatear("encendiendo el dispositivo");
    }

    public default void apagar() {
        formatear("apagando el dispositivo");
    }

    private static void formatear( String mensaje) {
        System.out.println("**********");
        System.out.println(mensaje);
        System.out.println("**********");
    }

    public void imprimir();
}
