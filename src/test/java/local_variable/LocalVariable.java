package local_variable;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LocalVariable {



    @Test
    public void localVariableLambda() {
        List<String> sampleList = Arrays.asList("Java", "Kotlin");
        String resultString = sampleList.stream()
                .map(( var x) -> x.toUpperCase())
                .collect(Collectors.joining(", "));
        Assertions.assertEquals("JAVA, KOTLIN", resultString);
    }

    @Test
    public void localVariable() {
        var list = List.of(1, 2, 3);
        var example = "example";
        Assertions.assertTrue(example instanceof String);
    }
}
