package strings;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StringNewMethods {

    @Test
    public void repeat() {
        System.out.println("=========== Repeat ==========");
        String repeat = "repeat ".repeat(3) + "3 times";
        System.out.println(repeat);
        Assertions.assertEquals("repeat repeat repeat 3 times", repeat);
    }

    /*
     * strip * () determina si el carácter es un espacio en blanco o no basado en Character.isWhitespace () . En otras palabras, es consciente de los caracteres de espacio en blanco Unicode .
     * Esto es diferente de trim () , que define el espacio como cualquier carácter que sea menor o igual al carácter de espacio Unicode (U + 0020). Si usamos trim () en el ejemplo anterior, obtendremos un resultado diferente:
     */
    @Test
    public void stripVsTrim() {
        System.out.println("=========== Strip Vs Trim ==========");
        String base = "\n\t  hello   \u2005";
        System.out.println("base");
        System.out.println(base);
        String baseStrip = base.strip();
        System.out.println("base strip");
        System.out.println(baseStrip);
        String baseTrim = base.trim();
        System.out.println("base trim");
        System.out.println(baseTrim);
        Assertions.assertEquals("hello", baseStrip);
    }

    /*
    isBlank funciona igual que strip
     */
    @Test
    public void blank() {
        System.out.println("=========== Blank ==========");
        boolean isBlank = "\n\t\u2005  ".isBlank();
        System.out.println(isBlank);
        Assertions.assertTrue(isBlank);
    }

    @Test
    public void multilines() {
        System.out.println("=========== Multilines ==========");
        String multilineStr = "This is\n \n a multiline\n rereturn \r string.";

        long lineCount = multilineStr.lines()
                .count();
        System.out.println(lineCount);
        Assertions.assertEquals(5L, lineCount);
    }

    @Test
    public void compareequals() {
        System.out.println("=========== equals with == ==========");
        boolean isEqual = "hola" == "hola";
        System.out.println(isEqual);
        Assertions.assertTrue(isEqual);
    }

}
