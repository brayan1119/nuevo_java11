package files_java11;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FilesJava11 {

    @Test
    public void createFileWhitString() {
        Path tempDir = Path.of("src","test","resources", "create_file_whit_string");
        String fileContent = null;
        try {
            Path filePath = Files.writeString(Files.createTempFile(tempDir, "demo", ".txt"), "Sample text");
            fileContent = Files.readString(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Assertions.assertEquals("Sample text", fileContent);
    }
}
