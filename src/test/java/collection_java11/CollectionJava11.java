package collection_java11;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CollectionJava11 {

    @Test
    public void listToArray() {
        List<String> sampleList = Arrays.asList("Java", "Kotlin");
        String[] sampleArray = sampleList.toArray(String[]::new);
        String[] sampleArrayExpect = {"Java", "Kotlin"};
        Assertions.assertArrayEquals(sampleArrayExpect, sampleArray);
    }

    @Test
    public void notPredicateMethod() {
        List<String> sampleList = Arrays.asList("Java", "\n \n", "Kotlin", " ");
        List withoutBlanksPredicateNot = sampleList.stream()
                .filter(Predicate.not(String::isBlank))
                .collect(Collectors.toList());
        List withoutBlanksNegate = sampleList.stream()
                .filter(value -> !value.isBlank())
                .collect(Collectors.toList());
        Assertions.assertArrayEquals(withoutBlanksPredicateNot.toArray(String[]::new), withoutBlanksNegate.toArray(String[]::new));
    }

    @Test
    public void newUnmodifiableMethod() {
        List<Integer> list = List.of(1, 2, 3);
        Set<String> set = Set.of("a", "b", "c");
        Stream<String> stream = Stream.of("a", "b", "c");
        Map<String, String> map = Map.of("clave 1", "valor 1", "clave 2",  "valor 2");

        List<Integer> listCopyOf = List.copyOf(list);
        Set<String> setCopyOf = Set.copyOf(set);
        Map<String, String> mapCopyOf = Map.copyOf(map);

        List toModifiableList = Stream.of("a", "b", "c").collect(Collectors.toList());
        toModifiableList.add("d");
        System.out.println(toModifiableList);
        List toUnmodifiableList = Stream.of("a", "b", "c").collect(Collectors.toUnmodifiableList());
        Set toUnmodifiableSet = Stream.of("g", "h", "i").collect(Collectors.toUnmodifiableSet());
        Map<Integer, Integer> toUnmodifiableMap = Stream.of(1, 2, 3).collect(Collectors.toUnmodifiableMap(
                num -> num,
                num -> num * 4));
        System.out.println(toUnmodifiableMap);
    }

    @Test
    public void newMethod() {
        System.out.println("=========== takeWhile ==========");
        List takeWhileResult = Stream.of(1, 2, 3, 4, 5).takeWhile(value -> value < 3).collect(Collectors.toList());
        System.out.println(takeWhileResult);

        System.out.println("=========== dropWhile ==========");
        List dropWhileResult = Stream.of(1, 2, 3, 4, 5).dropWhile(value -> value < 3).collect(Collectors.toList());
        System.out.println(dropWhileResult);

        System.out.println("=========== iterate ==========");
        List iterateResult = Stream.iterate(1L, n  ->  n  + 1).limit(10).collect(Collectors.toList());
        System.out.println(iterateResult);

        System.out.println("=========== ofNullable no null ==========");
        String example = "example";
        List ofNullableResult = Stream.ofNullable(example).collect(Collectors.toList());
        System.out.println(ofNullableResult);
        System.out.println("=========== ofNullable null ==========");
        String nullExample = null;
        List ofNullableNullResult = Stream.ofNullable(nullExample).filter(String::isBlank).collect(Collectors.toList());
        System.out.println(ofNullableNullResult);
        List ofNullResult = Stream.of(nullExample).filter(String::isBlank).collect(Collectors.toList());// non compile
        System.out.println(ofNullResult);
    }
}
