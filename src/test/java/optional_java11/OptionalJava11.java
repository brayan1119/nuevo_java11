package optional_java11;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OptionalJava11 {

    @Test
    public void streamTest() {
        Optional<String> optional1 = Optional.of("Texto 1");
        Optional<String> optional2 = Optional.empty();
        Optional<String> optional3 = Optional.of("Texto 3");

        List<String> textos = Stream.of(optional1, optional2, optional3)
                .flatMap(Optional::stream).collect(Collectors.toList());

        Assertions.assertEquals(2, textos.size());
        Assertions.assertEquals("Texto 1", textos.get(0));
        Assertions.assertEquals("Texto 3", textos.get(1));

    }

    @Test
    public void ifPresentOrElseTest() {
        Optional<String> optionalConUnTexto = Optional.of("Un texto");
        Optional<String> optionalVacio = Optional.empty();

        optionalConUnTexto.ifPresentOrElse(
                texto ->{
                    System.out.println("El optional con texto debería pasar por el present.");
                    Assertions.assertEquals("Un texto", texto);
                },
                () -> {
                    throw new RuntimeException("El optional con texto nunca debería pasar por el else");
                }
        );

        optionalVacio.ifPresentOrElse(
                texto -> {
                    throw new RuntimeException("El optional vacío nunca debería pasar por el present");
                },
                () -> System.out.println("El optional vacío debería pasar por el else")
        );
    }

    @Test
    public void orTest() {
        Optional<String> optionalConUnTexto = Optional.of("Un texto");
        Optional<String> optionalVacio = Optional.empty();

        Optional orOptionalConUnTexto = optionalConUnTexto.or(()->Optional.of("Optional vacío"));
        Optional orOptionalVacio = optionalVacio.or(()->Optional.of("Optional vacío"));
        String vacio = optionalVacio.orElse("Texto vacio directo");

        Assertions.assertEquals("Un texto", orOptionalConUnTexto.get());
        Assertions.assertEquals("Optional vacío", orOptionalVacio.get());
        Assertions.assertEquals("Texto vacio directo", vacio);
    }

    @Test
    public void orElseThrowTest() {
        Optional<String> optionalVacio = Optional.empty();
        try {
            optionalVacio.orElseThrow(() -> new Exception("El optional era nulo"));
        } catch (Exception e) {
            Assertions.assertEquals("El optional era nulo", e.getMessage());
        }
    }
}
